#! /usr/bin/env python3

import json
import os
import requests
import time


class Experian(object):
    def __init__(self):
        self.env = ""
        self.username = ""
        self.password = ""
        self.client_id = ""
        self.client_secret = ""
        self.token = None
        self.token_fetched_at = time.time()
        self.url = ""
        self.expected_keys = {
            "name": ["firstName", "lastName", "middleName", "generationCode"],
            "dob": ["dob"],
            "ssn": ["ssn"],
            "currentAddress": ["line1", "line2", "city", "state", "zipCode"]
        }

    def load_config(self, conf_file="config_experian.json"):
        """
        loads configurations from the given config file

        :param self:
        :param conf_file: (str) config file containing user and application info
        :return: (bool) true if config file loaded successfully
        """
        if not os.path.exists(conf_file):
            return False

        # If config file exists read the file and initialize the variables
        with open(conf_file) as fobj:
            content = json.loads(fobj.read())

        # Initialize the variables using loaded config file
        self.env = content["env"]
        self.username = content["username"]
        self.password = content["password"]
        self.client_id = content["Client_id"]
        self.client_secret = content["Client_secret"]

        urls = {
            "sandbox": "https://sandbox-us-api.experian.com/",
            "prod": "https://us-api.experian.com/"
        }
        self.url = urls.get(self.env, None)

        if not self.url:
            return False

        return True

    def get_token(self):
        """
        checks if valid token already exists else it
        fetches one from the Experian server

        :return: (bool) true if obj has valid token
        """
        if self.env == "":
            raise Exception("Cofiguration has not been loaded yet")
        token_url = self.url + "oauth2/v1/token"

        payload = {
            "username": self.username,
            "password": self.password,
        }

        headers = {
            "Client_id": self.client_id,
            "Client_secret": self.client_secret,
            "accept": "application/json",
            "content-type": "application/json",
            "refresh_token": ""
        }

        if self.token:
            # If token is expired reissue a token using the refresh_token
            if self.token_fetched_at + float(self.token["expires_in"]) < time.time():
                # Add the refresh_token header
                headers["refresh_token"] = self.token["refresh_token"]

            # Return True to use previous token as it is not expired yet
            else:
                return True

        resp = requests.post(token_url, headers=headers, json=payload)

        print(str(resp.status_code) + "\n")
        print(resp.json())
        if resp.status_code != 200:
            self.token = None
            return False

        self.token = resp.json()
        self.token_fetched_at = time.time()

        return True

    def get_creditscore(self, usr_data: dict):
        """
        It checks the user provided data and sends
        to Experian to fetch user's credit score

        :param usr_data: <dict> This param must contain following keys
            {
                "name": {
                    "lastName": "LAST_NAME",
                    "firstName": "FIRST_NAME",
                    "middleName": "MIDDLE_NAME",
                    "generationCode": "",
                },
                "dob": {
                    "dob": "ddmmyyyy"
                },
                "ssn": {
                    "ssn": "999999990"
                },
                "currentAddress": {
                    "line1": " ",
                    "line2": " ",
                    "city": " ",
                    "state": " ",
                    "zipCode": " "
                }
            }
        :return: <int> credit score would be returned
        """
        if not self.check_usr_data(usr_data):
            raise Exception("User data is not valid")

        payload = {
            "primaryApplicant": usr_data,
            "requestor": {
                "subscriberCode": "5991764"
            },
            "permissiblePurpose": {
                "type": "3F"
            },
            "addOns": {
                "riskModels": {
                    "modelIndicator": ["F","V"]
                }
            }
        }

        headers = {
            "accept": "application/json",
            "content-type": "application/json",
            "authorization": "Bearer " + self.token["access_token"]
        }

        req_url = self.url + "consumerservices/prequal/v1/credit-score"
        resp = requests.post(req_url, headers=headers, json=payload)

        print(str(resp.status_code) + "\n")
        print(resp.json())
        return resp.json()["customSolution"][0]["riskModel"][0]["score"]

    def check_usr_data(self, usr_data):
        """
        Checks if user_data dict contains expected keys
        :param usr_data: <dict>
        :return: <bool> True if expected keys are present in the usr_data
        """
        for key in self.expected_keys:
            if key in usr_data:
                for k in self.expected_keys[key]:
                    if k not in usr_data[key]:
                        return False
            else:
                return False
        return True

experian = Experian()
